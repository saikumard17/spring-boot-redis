package com.sai.resources;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/helloWorld")
public class HelloResource {
	
	
	@GetMapping("/chitti")
	public String helloChitti()
	{
		return "Hello world. I am chitti";
	}

}
